<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

//Route::get('/', function() {
    // if(Input::has('name'))
    //  return Input::get('name');
    // return Request::method();
    //return View::make('hello');
    //$value = 'ssss';
    //return Response::view('hello')->header('content-type', $value);
//});

//Route::get('nerds', ['uses' => 'NerdController@index']);


//Route::resource('user', 'UserController');



// Route::controller('/', 'HomeController'); 
 Route::resource('nerds', 'NerdController');


// Route::post('foo/bar', function() {
//     return Input::get('username');
// });

// Route::match(array('GET', 'POST'), '/abc', function() {
//     return 'Hello World bcd';
// });

// Route::get('user', array('before' => 'old', function() {
//     return 'you are over than 200 year olds!';
// }));

// Route::get('/', function() {
    
//     return View::make('pages.home', array('name1' => 'Quan', 'birth' => '1987'));
// });

// Route::get('about', function() {

//     return View::make('pages.about');
// });

// Route::get('projects', function() {
//     return View::make('pages.projects');
// });

// Route::get('contacts', function() {
//     return View::make('pages.contacts');
// });

Route::get('/', function() {
    $data = array();

    return URL::route('nerds.index', $data);
});
